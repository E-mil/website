import React from 'react'
import { contentboxStyle, contentboxTitleStyle, contentboxItemStyle } from '../default_styles/styles'

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

class startScreen extends React.Component {

  constructor(props) {
    super(props)
    this.canvasRef = React.createRef()
    this.date = new Date()
    this.lastTimeStamp = this.date.getTime()
    this.state = { countdown: 0, balls: [] }
    this.updateAnimationState = this.updateAnimationState.bind(this)
    window.WIDTH = 2250
    window.HEIGHT = 1750
  }

  componentDidMount() {
    this.rAF = requestAnimationFrame(this.updateAnimationState)
  }

  async updateAnimationState() {
    let timeStamp = this.date.getTime()
    let diff = timeStamp - this.lastTimeStamp
    if (diff < 25) {
        await sleep(diff)
    }
    this.lastTimeStamp = this.date.getTime()
    let cd = this.state.countdown
    cd = cd - 1
    if (cd < 0) {
        this.addNewBall()
        cd = Math.random() * 40 + 10
    }
    let removeList = []
    for (let ball of this.state.balls) {
        if (ball.update()) {
            removeList.push(ball)
        }
    }
    this.state.balls = this.state.balls.filter(function(value, index, arr) {
        for (let entry of removeList) {
            if (entry === value) {
                return false
            }
        }
        return true
    })
    this.setState({ countdown: cd })
    this.rAF = requestAnimationFrame(this.updateAnimationState)
  }

  addNewBall() {
    let r1 = Math.random()
    let x = 0
    let y = 0
    let dx = 0
    let dy = 0
    let radius = Math.random() * 75 + 25
    let color = "rgb(" + parseInt(Math.random() * 255) + ", " +
        parseInt(Math.random() * 255) + ", " + parseInt(Math.random() * 255) + ")"
    let velocity = Math.random() * 1.5 + 1
    if (r1 < 0.5) {
        if (r1 < 0.25) { //top
            x = r1 * 4 * window.WIDTH
            y = 0
            dy = 1
            dx = (window.WIDTH / 2 - x) / window.HEIGHT * 2
        } else { //bottom
            x = (r1 - 0.25) * 4 * window.WIDTH
            y = window.HEIGHT
            dy = -1
            dx = (window.WIDTH / 2 - x) / window.HEIGHT * 2
        }
    } else {
        if (r1 < 0.75) { //left
            x = 0
            y = (r1 - 0.5) * 4 * window.HEIGHT
            dy = (window.HEIGHT / 2 - y) / window.WIDTH * 2
            dx = 1
        } else { //right
            x = window.WIDTH
            y = (r1 - 0.75) * 4 * window.HEIGHT
            dy = (window.HEIGHT / 2 - y) / window.WIDTH * 2
            dx = -1
        }
    }
    let n = Math.sqrt(1/(dx*dx + dy*dy))
    dx = n*dx * velocity
    dy = n*dy * velocity
    this.state.balls.push(new Ball({
            radius: radius,
            x: x,
            y: y,
            dx: dx,
            dy: dy,
            opacity: 0,
            color: color,
            velocity: velocity
        }))
  }

  componentWillUnmount() {
    cancelAnimationFrame(this.rAF)
  }

  componentDidUpdate() {
    this.paint()
  }

  paint() {
    const canvas = this.canvasRef.current
    const context = canvas.getContext('2d')
    const width = canvas.width
    const height = canvas.height
    context.save()
    context.fillStyle = '#FFFCE2'
    context.fillRect(0, 0, width, height)
    context.fillStyle = 'black'
    for (let ball of this.state.balls) {
      context.fillStyle = ball.state.color
      context.globalAlpha = ball.state.opacity
      context.beginPath()
      context.ellipse(ball.state.x, ball.state.y, ball.state.radius, ball.state.radius, 0, 0, 2 * Math.PI)
      context.fill()
    }
    context.globalAlpha = 1
    context.restore()
  }

  render() {
    return (
      <div style={Object.assign({}, contentboxStyle, { alignItems: "center" })}>
        <p align="center" style={contentboxTitleStyle}>Hi!</p>
        <p align="center" style={contentboxItemStyle}>
        I am Emil. This is my website.
        </p>
        <canvas ref={this.canvasRef} width={window.WIDTH} height={window.HEIGHT}
        style={{ width: "45vw", height: "35vw", marginBottom: "2vw", marginTop: "2vw" }} />
      </div>
    )
  }
}

class Ball {

  constructor(props) {
    this.state = props
    this.mass = this.state.radius * this.state.radius * Math.PI
  }

  update() {
    let newX = this.state.x + this.state.dx
    let newY = this.state.y + this.state.dy
    let dx = newX - window.WIDTH / 2
    let dy = newY - window.HEIGHT / 2
    let opacity = (window.HEIGHT / 2 - Math.sqrt(dx * dx + dy * dy) - 35) / window.HEIGHT * 2
    if (opacity < 0) {
        opacity = 0
    }
    this.state = Object.assign({}, this.state, { x: newX, y: newY, opacity: opacity })
    if (newX < 0 || newX > window.WIDTH || newY < 0 || newY > window.HEIGHT) {
        return true
    } else {
        return false
    }
  }

  updateState(newstate) {
    this.state = Object.assign({}, this.state, newstate)
  }
}

export default startScreen