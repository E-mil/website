import React from 'react'
import { contentboxStyle, contentboxTitleStyle, contentboxItemStyle } from '../default_styles/styles'

const art = () => {
  return (
    <div style={contentboxStyle}>
      <p style={contentboxTitleStyle}>Art</p>
      <img style={{ height: "40vh", width: "40vw", marginTop: "3%" }} src={require("../res/bear.png")}
        alt="Seems like you can't see this pretty bear"/>
      <p style={contentboxItemStyle}>pretty.</p>
    </div>
  )
}

export default art