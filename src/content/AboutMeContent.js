import React from 'react'
import { contentboxStyle, contentboxTitleStyle, contentboxItemStyle } from '../default_styles/styles'

const aboutMe = () => {
  return (
    <div style={contentboxStyle}>
      <p style={contentboxTitleStyle}>About me</p>
      <p style={contentboxItemStyle}>
        My name is Emil Bengtsson. I am currently pursuing a Master of Science in Computer Science and Engineering
        at Lund University, Sweden.
      </p>
      <p style={Object.assign({}, contentboxItemStyle, {marginTop: 0})}>
        Right now I'm in very last part of my education, the thesis. I am writing it at Volvo Cars in
        Lund with a friend. The title for the thesis will probably be something like: <i>memory profiling and leak
        detection in a constrained embedded linux system</i>.
      </p>
    </div>
  )
}

export default aboutMe