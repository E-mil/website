import React from 'react'
import { contentboxStyle, contentboxTitleStyle, contentboxItemStyle, linkStyle } from '../default_styles/styles'
import report from '../res/Multicore_Report.pdf'

const projects = () => {
  return (
    <div style={contentboxStyle}>
      <p style={contentboxTitleStyle}>Projects</p>
      <p style={contentboxItemStyle}>
        Some stuff I've been working on.
        <p style={Object.assign({}, contentboxItemStyle, { fontWeight: "bold" })}>Website</p>
        This website is written in Reactjs by me, the code is avaliable&nbsp;
        <a href="https://bitbucket.org/E-mil/website/src" target="_blank" rel="noopener noreferrer" style={linkStyle}>
          here
        </a>.
        <p style={Object.assign({}, contentboxItemStyle, { fontWeight: "bold" })}>OpenAI</p>
        I have been working a little with OpenAI gym,
        trying to understand reinforcement learning better.&nbsp;
        <a href="https://github.com/openai/gym/wiki/CartPole-v0" target="_blank" rel="noopener noreferrer" style={linkStyle}>
          Here
        </a>
        &nbsp;is a link to the "cartpole" problem, which I have been working on, and&nbsp;
        <a href="https://bitbucket.org/E-mil/openai/src" target="_blank" rel="noopener noreferrer" style={linkStyle}>
          here
        </a>
        &nbsp;is some code I wrote for solving it.
        <p style={Object.assign({}, contentboxItemStyle, { fontWeight: "bold" })}>Courses at Virginia Tech</p>
        I studied at Virginia Tech during the fall of 2018, and when I was there I took some interesting classes where
        we got to do a lot of assignments and projects. Most of the work I did during my time abroad can be found&nbsp;
        <a href="https://github.com/EmilBengtsson2/VT-coursework" target="_blank" rel="noopener noreferrer" style={linkStyle}>
          here
        </a>.
        <p style={Object.assign({}, contentboxItemStyle, { fontWeight: "bold" })}>Parallel Breadth-First Search</p>
        I took a class where I got to implement breadth-first search in parallel, and optimize it.&nbsp;
        <a href="https://bitbucket.org/E-mil/multicoreprojekt/src/master/" target="_blank" rel="noopener noreferrer" style={linkStyle}>
          Here
        </a> is some code I wrote and&nbsp;
        <a href={report} target="_blank" rel="noopener noreferrer" style={linkStyle}>
          here
        </a> is a report on my findings.
      </p>
    </div>
  )
}

export default projects