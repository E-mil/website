import React from 'react'
import { contentboxStyle, contentboxTitleStyle, contentboxItemStyle, linkStyle } from '../default_styles/styles'

const coolWebsites = () => {
  return (
    <div style={contentboxStyle}>
      <p style={contentboxTitleStyle}>Cool Websites</p>
      <p style={contentboxItemStyle}>
        Isabel is a cool person :) Her website is&nbsp;
        <a href="https://www.isabelnewsome.com" target="_blank" rel="noopener noreferrer" style={linkStyle}>
          isabelnewsome.com
        </a>.
      </p>
      <p style={Object.assign({}, contentboxItemStyle, {marginTop: 0})}>
        <a href="https://www.wikipedia.org" target="_blank" rel="noopener noreferrer" style={linkStyle}>
          This
        </a>&nbsp;is probably the coolest website on the internet.
      </p>
    </div>
  )
}

export default coolWebsites