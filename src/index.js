import React from 'react';
import ReactDOM from 'react-dom';
import './css_styles/index.css';
import App from './pages/MainPage';

ReactDOM.render(<App />, document.getElementById('root'));
