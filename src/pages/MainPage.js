import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import { pageStyle, sidebarStyle, sidebarTitleStyle, sidebarItemStyle, iconStyle,
  iconBoxStyle } from '../default_styles/styles'
import startScreen from '../content/StartScreenContent'
import projects from '../content/ProjectsContent'
import coolWebsites from '../content/CoolWebsitesContent'
import aboutMe from '../content/AboutMeContent'
import bitbucket from '../res/icons/_ionicons_svg_logo-bitbucket.svg'
import github from '../res/icons/_ionicons_svg_logo-github.svg'
import linkedin from '../res/icons/_ionicons_svg_logo-linkedin.svg'

class App extends Component {

  render() {
    return (
      <Router>
        <div style={pageStyle}>
          <div style={Object.assign({}, sidebarStyle, { padding: "1vw" })}>
            <div style={sidebarStyle}>
              <Link style={sidebarTitleStyle} to="/">Emil Bengtsson</Link>
              <Link style={sidebarItemStyle} to="/projects">Projects</Link>
              <Link style={sidebarItemStyle} to="/websites">Cool Websites</Link>
              <Link style={sidebarItemStyle} to="/aboutme">About Me</Link>
            </div>
            <div style={iconBoxStyle}>
              <a href="https://www.bitbucket.org/E-mil/" target="_blank" rel="noopener noreferrer" style={iconStyle}>
                <img src={bitbucket} alt="bitbucket" title="Bitbucket" />
              </a>
              <a href="https://www.github.com/EmilBengtsson2" target="_blank" rel="noopener noreferrer" style={iconStyle}>
                <img src={github} alt="github" title="GitHub" />
              </a>
              <a href="https://www.linkedin.com/in/emil-bengtsson-051a9214b/" target="_blank" rel="noopener noreferrer" style={iconStyle}>
                <img src={linkedin} alt="linkedin" title="LinkedIn" />
              </a>
            </div>
          </div>
          <Route exact path="/" component={startScreen} />
          <Route path="/projects" component={projects} />
          <Route path="/websites" component={coolWebsites} />
          <Route path="/aboutme" component={aboutMe} />
        </div>
      </Router>
    )
  }
}

export default App
