const pageStyle = {
    height: "100vh",
    width: "100vw",
    backgroundColor: "#FFFCE2",
    padding: "1vw",
    flexDirection: "row",
    boxSizing: "border-box",
    display: "flex"
}

const sidebarStyle = {
    flexDirection: "column",
    boxSizing: "border-box",
    display: "flex",
    backgroundColor: "#FFEAA8",
    flex: 1,
    borderRadius: "1.2vw"
}

const sidebarTitleStyle = {
    fontSize: "220%",
    margin: 0,
    padding: "2%",
    paddingBottom: "3%",
    fontWeight: "bold",
    fontFamily: "cantarell, sans-serif",
    border: 0,
    backgroundColor: "Transparent",
    textAlign: "left",
    outline: "none",
    cursor: "pointer",
    color: "black",
    textDecorationLine: "none"
}

const sidebarItemStyle = {
    fontSize: "115%",
    margin: "1%",
    marginBottom: 0,
    padding: "4%",
    fontWeight: "bold",
    fontFamily: "cantarell, sans-serif",
    border: 0,
    backgroundColor: "Transparent",
    textAlign: "left",
    outline: "none",
    cursor: "pointer",
    color: "black",
    textDecorationLine: "none"
}

const contentboxStyle = {
    flexDirection: "column",
    boxSizing: "border-box",
    display: "flex",
    backgroundColor: "#FFFCE2",
    flex: 3,
    paddingTop: "4vw",
    paddingLeft: "15vw",
    paddingRight: "15vw"
}

const contentboxTitleStyle = {
    fontSize: "150%",
    margin: 0,
    fontWeight: "bold",
    fontFamily: "cantarell, sans-serif"
}

const contentboxItemStyle = {
    fontSize: "110%",
    marginTop: "3%",
    fontFamily: "cantarell, sans-serif"
}

const linkStyle = {
    color: "#ff0000"
}

const iconBoxStyle = {
    flexDirection: "row",
    boxSizing: "border-box",
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center"
}

const iconStyle = {
    width: "15%",
    height: "15%",
    cursor: "pointer",
    marginBottom: "5%"
}

export { pageStyle, sidebarStyle, contentboxStyle, sidebarTitleStyle, sidebarItemStyle
    , contentboxTitleStyle, contentboxItemStyle, linkStyle, iconStyle, iconBoxStyle }